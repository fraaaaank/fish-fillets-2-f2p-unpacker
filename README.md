# Fish Fillets 2 F2P unpacker

Decrypt and unpack resource files from Fish Fillets 2, the **best video game ever made**. This includes scripts, maps, movies, voice acting...

## Building

You can build with VS2013 and probably with later VS versions also. It requires Crypto++ 8.2, which should come along as a git module automatically.

## Usage

Just copy F2P files ("packets") into the same directory as the program and run it.

Both official (parceled with the game) and user-created packets are supported.

It also supports stats (.bin) and save files (.f2s) (check the Profiles directory in the program location).

Enjoy!
