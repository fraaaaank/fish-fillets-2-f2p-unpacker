//// f2p_unpacker.cpp : Unpack Fish Fillets 2 F2P files.
////

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <filesystem>
#include <vector>
#include <string>
#include <sstream>
#include <exception>
#include <array>
#include <map>

#include "modes.h"
#include "aes.h"
#include "filters.h"
#include "hex.h"
#include "sha.h"
#include "hmac.h"
#include "files.h"

#include "windows.h"
#include "fileapi.h"
#include <io.h>

const static char f2p_header[] = "061212";
const static size_t MAIN_STATS_HEADER_LEN = 3u;
const static size_t SAVES_BONUS_STATS_HEADER_LEN = 10u;
static const size_t KEY_SIZE = 32u;

using Key = std::array<CryptoPP::byte, KEY_SIZE>;
enum class Keys {
	LISTING_OFFICIAL,
	DATA_OFFICIAL,
	LISTING_CUSTOM,
	DATA_CUSTOM,
	REGISTRY_KEYS,
	MAIN_STATS,
	SAVES_BONUS_STATS
};

// a bunch of keys are used for different stuff...
// the strings used to derive don't correlate with what they're used for.
static std::map<Keys, std::string> keyBaseMap{
	{ Keys::LISTING_OFFICIAL,	"BEPAVCRoomGrRepresentation@@XZV1"			 },
	{ Keys::DATA_OFFICIAL,		"P8CRoomGrRepresentation@@AEXMM"			 },
	{ Keys::LISTING_CUSTOM,		"P8CGrElSprite@@AEX_N@ZV1@Unull_type"		 },
	{ Keys::DATA_CUSTOM,		"P8CGrElSpriteSet@@AEPAV1@XZV1@Unull_type"	 },
	{ Keys::REGISTRY_KEYS,		"@@AEXPAVCRoomObject@@PAVCGrElContainer"	 },
	{ Keys::MAIN_STATS,			"Unull_type@detail@luabind@@@detail@luabind" },
	{ Keys::SAVES_BONUS_STATS,	"P8CRoomGrRepresentation@@BEHXZV1"			 }
};

class KeyMap {
	std::map<Keys, Key> keys;

public:
	KeyMap();
	inline const Key& at(Keys k) {
		return keys.at(k);
	}
};

KeyMap::KeyMap() {
	for (auto& keyBase : keyBaseMap) {
		auto newKey = keys.insert(std::make_pair(keyBase.first, Key{})).first;
		CryptoPP::SHA256().CalculateDigest(newKey->second.data(),
			reinterpret_cast<const CryptoPP::byte *>(keyBase.second.c_str()),
			keyBase.second.length());
	}
}

static KeyMap keyMap;

struct Fillets2EncryptedObj {
private:
	const CryptoPP::byte * data;
	size_t data_len;

public:
	inline Fillets2EncryptedObj(const CryptoPP::byte * d, size_t l) :
		data(d), data_len(l)
	{
	}
	inline void setData(const CryptoPP::byte * ptr, size_t len) {
		data = ptr; data_len = len;
	}
	bool decrypt(const Key& key, std::ostream& out);
};

bool Fillets2EncryptedObj::decrypt(const Key& key, std::ostream& out) {
	static const size_t iv_base_size = 8u;
	static const size_t hmac_size = 20u;

	if (data_len < hmac_size + iv_base_size) {
		throw std::exception("Incomplete data");
	}

	const size_t encrypted_data_len = data_len - hmac_size - iv_base_size;
	const CryptoPP::byte * hmac = data + data_len - hmac_size;
	const CryptoPP::byte * iv_base = hmac - iv_base_size;

	// generate the iv, why they don't just append it instead.
	// pointless obfuscation?
	static CryptoPP::byte sha_digest[CryptoPP::SHA256::DIGESTSIZE];
	static CryptoPP::byte iv[16];
	CryptoPP::SHA256().CalculateDigest(sha_digest, iv_base, iv_base_size);
	static_assert(sizeof(sha_digest) == sizeof(iv) * 2, "");
	for (size_t i = 0; i < sizeof(iv); i++) {
		iv[i] = sha_digest[i] ^ sha_digest[sizeof(iv) + i];
	}

	// now test the key (authenticate teh HMAC)
	CryptoPP::HMAC<CryptoPP::SHA1> h{ key.data(), key.size() };
	static CryptoPP::byte hmac_digest[h.DIGESTSIZE];
	static_assert(h.DIGESTSIZE == hmac_size, "");
	h.Update(data, data_len - hmac_size);
	h.Final(hmac_digest);
	if (!std::equal(hmac, hmac + hmac_size, hmac_digest)) {
		return false;
	}

	// key is correct, we 're good to decrypt!!
	CryptoPP::CFB_Mode<CryptoPP::AES>::Decryption cfbDecryption;
	cfbDecryption.SetKeyWithIV(key.data(), key.size(), iv, sizeof(iv));
	CryptoPP::StringSource dataSource(data, encrypted_data_len, false);
	CryptoPP::StreamTransformationFilter stfDecryptor(cfbDecryption);
	dataSource.Attach(new CryptoPP::Redirector(stfDecryptor));

	// we decrypt one byte, and from this can derive how many mysterious
	// useless bytes to skip before the real data starts
	CryptoPP::byte firstByte;
	stfDecryptor.Attach(new CryptoPP::ArraySink(
		&firstByte, sizeof(firstByte)));
	if (dataSource.Pump(sizeof(firstByte)) != sizeof(firstByte)) {
		throw std::exception("Failed to decrypt first byte");
	}
	const size_t uselessBytesToSkip = (firstByte & 0xF) + 2u;
	stfDecryptor.Detach(nullptr);

	// skip the useless bytes
	if (dataSource.Pump(uselessBytesToSkip) != uselessBytesToSkip) {
		throw std::exception("Failed to skip mysterious header");
	}

	// attach our ostream and decrypt everything else
	stfDecryptor.Attach(new CryptoPP::FileSink(out));
	dataSource.PumpAll();
	return true;
}

struct F2PEntry {
	std::string name{};

	inline F2PEntry(std::string n) :
		name(n)
	{
	}

	inline static std::string extractName(const CryptoPP::byte * src,
										  size_t limit) {
		const char * src_cast = reinterpret_cast<const char *>(src);
		// safety check
		const size_t check_limit = strnlen_s(src_cast, limit);
		if (strnlen_s(src_cast, limit) == limit) {
			throw std::exception("Out of bounds");
		}
		return std::string(src_cast);
	}
};

struct F2PFile : public Fillets2EncryptedObj, public F2PEntry
{
	inline F2PFile(std::string n, const CryptoPP::byte * d, size_t l) :
		Fillets2EncryptedObj(d, l), F2PEntry(n)
	{
	}
};

struct F2PDir : public F2PEntry {
	std::vector<F2PDir> subdirs;
	std::vector<F2PFile> files;

	inline F2PDir(std::string n)
		: F2PEntry(n)
	{
	}

	void decryptAndWrite(const Key& key, std::string path);
};

void F2PDir::decryptAndWrite(const Key& key, std::string path) {
	CreateDirectoryA(path.c_str(), NULL);
	for (auto& it : files) {
		const std::string filepath = path + "\\" + it.name;
		std::cerr << filepath << std::endl;
		std::ofstream out(path + "\\" + it.name, std::ios::binary);
		bool res = it.decrypt(key, out);

		if (!res) {
			throw std::exception("HMAC auth for data failed (wrong key?)");
		}
	}
	for (auto& it : subdirs) {
		it.decryptAndWrite(key, path + "\\" + it.name);
	}
}

struct FileWrapper {
	std::string filename{};
	size_t filesize{};
	HANDLE file_handle{};
	HANDLE file_mapping{};
	const CryptoPP::byte * file_view{};

	void openFile(const CHAR * filename);

	virtual ~FileWrapper();
};

void FileWrapper::openFile(const CHAR * filename) {
	this->filename = filename;

	file_handle = CreateFileA(filename, GENERIC_READ, 0x0, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (file_handle == INVALID_HANDLE_VALUE) {
		throw std::exception("Failed to open file");
	}

	filesize = GetFileSize(file_handle, NULL);

	file_mapping = CreateFileMapping(file_handle, NULL, PAGE_READONLY, 0, 0,
		NULL);
	if (file_mapping == NULL) {
		throw std::exception("Failed to create filemap");
	}

	file_view = (const CryptoPP::byte *)MapViewOfFile(file_mapping,
		FILE_MAP_READ, 0, 0, 0);
	if (file_view == NULL) {
		throw std::exception("Failed to create view of filemap");
	}
}

FileWrapper::~FileWrapper() {
	if (file_view) {
		UnmapViewOfFile(file_view);
	}
	if (file_mapping) {
		CloseHandle(file_mapping);
	}
	if (file_handle) {
		CloseHandle(file_handle);
	}
}

struct F2PWrapper : public FileWrapper {
	uint32_t listing_offset{};
	CryptoPP::byte * listing{};
	size_t listing_len{};
	bool isOfficial{}; // is this official F2P or custom?

	void checkHeader();
	void decryptListing();

	F2PDir parseF2PDir(size_t& ind);
	inline F2PDir parseF2PListing() {
		size_t ind = 0u;
		return parseF2PDir(ind);
	}

	~F2PWrapper() override;
};

F2PWrapper::~F2PWrapper() {
	if (listing) {
		delete[] listing;
	}
}

void F2PWrapper::checkHeader() {
	if (filesize < sizeof(f2p_header) + sizeof(uint32_t)) {
		throw std::exception("Insufficient filesize");
	}
	for (size_t i = 0; i < sizeof(f2p_header); i++) {
		if (file_view[i] != f2p_header[i])
			throw std::exception("Incorrect header");
	}
	listing_offset = *(const uint32_t *)(file_view + sizeof(f2p_header));
	if (listing_offset >= filesize) {
		throw std::exception("Out-of-bounds listing index");
	}
}

void F2PWrapper::decryptListing() {
	std::cerr << "Decrypting listing..." << std::endl;
	Fillets2EncryptedObj listing_obj{ file_view + listing_offset,
		filesize - listing_offset };
	std::stringstream strm;

	bool res = listing_obj.decrypt(keyMap.at(Keys::LISTING_OFFICIAL), strm);
	if (res) {
		isOfficial = true;
	}
	else {
		res = listing_obj.decrypt(keyMap.at(Keys::LISTING_CUSTOM), strm);
		isOfficial = false;
		if (!res) {
			throw std::exception("HMAC auth for listing failed (wrong key?)");
		}
	}

	auto listing_str = strm.str();
	listing = new CryptoPP::byte[listing_str.length()];
	std::copy(listing_str.begin(), listing_str.end(), listing);
	listing_len = listing_str.length();
}

F2PDir F2PWrapper::parseF2PDir(size_t& ind) {
	bool ranOutOfSpace = false;
	auto grabInt = [&]() {
		if (ind + sizeof(uint32_t) > listing_len) {
			throw std::exception("Out of bounds");
		}
		uint32_t x = *(const uint32_t*)(listing + ind);
		ind += sizeof(uint32_t);
		return x;
	};

	auto name = F2PEntry::extractName(listing + ind, listing_len - ind);
	ind += name.length() + 1; // add null terminator
	F2PDir dir{name};
	uint32_t numSubDirs = grabInt();
	while (numSubDirs--) {
		dir.subdirs.push_back(parseF2PDir(ind));
	}
	uint32_t numFiles = grabInt();
	while (numFiles--) {
		auto name = F2PEntry::extractName(listing + ind, listing_len - ind);
		ind += name.length() + 1; // add null terminator
		uint32_t fileLoc = grabInt();
		uint32_t fileLen = grabInt();
		if (fileLoc > filesize || fileLoc + fileLen > filesize) {
			throw std::exception("Out of bounds");
		}
		dir.files.push_back(F2PFile(name, file_view + fileLoc, fileLen));
	}
	return dir;
}

std::vector<std::string> getFilenames(const char * filter) {
	std::vector<std::string> f2p_filenames;
	struct _finddata_t c_file;
	intptr_t hFile;

	if ((hFile = _findfirst(filter, &c_file)) == -1L)
		return f2p_filenames;
	
	do {
		f2p_filenames.push_back(c_file.name);
	} while (_findnext(hFile, &c_file) == 0);
	_findclose(hFile);

	return f2p_filenames;
}

struct MiscWrapper : public FileWrapper {
public:
	void decryptAndWrite(const Key& key, std::string path, size_t header_len);
};

void MiscWrapper::decryptAndWrite(const Key& key, std::string path,
								   size_t header_len) {
	// skip stats/saves header ... a versioning system of some kind
	if (filesize <= header_len) {
		throw std::exception("Insufficient filesize");
	}

	Fillets2EncryptedObj stats{ file_view + header_len,
		filesize - header_len };

	std::ofstream out(path, std::ios::binary);
	bool res = stats.decrypt(key, out);

	if (!res) {
		throw std::exception("HMAC auth failed (wrong key?)");
	}
}

void lineSeparate() {
	static bool first = true;
	if (first)
		first = false;
	else
		std::cerr << std::endl;
}

void decryptNonF2P(std::vector<std::string>& filenames,
				   std::string typeOfFiles,
				   std::string dirName,
				   Keys keyToUse,
				   size_t header_len)
{
	if (filenames.size() == 0u) {
		lineSeparate();
		std::cerr << "No " << typeOfFiles << " files found." << std::endl;
	}
	else {
		lineSeparate();
		CreateDirectoryA(dirName.c_str(), NULL);
		std::cerr << "Begin decrypting " << typeOfFiles << " files..." << std::endl;
		for (auto& filename : filenames) {
			MiscWrapper wrapper;

			std::cerr << filename << std::endl;

			try {
				wrapper.openFile(filename.c_str());
				wrapper.decryptAndWrite(keyMap.at(keyToUse),
					dirName + "\\" + filename, header_len);
			}
			catch (std::exception& e) {
				std::cerr << "Error: " << e.what() << std::endl;
			}

		}
		std::cerr << "Finished decrypting " << typeOfFiles << " files" << std::endl;
	}
}

int main(int argc, char* argv[]) {
	auto F2Pfilenames = getFilenames("*.f2p");
	auto statsFilenames = getFilenames("stats*.bin");
	auto hintBonusStatsFileNames = getFilenames("hint_stats.bin");
	auto bonusStatsFileNames = getFilenames("bonus_stats.bin");
	hintBonusStatsFileNames.insert(hintBonusStatsFileNames.end(),
		bonusStatsFileNames.begin(), bonusStatsFileNames.end());
	auto saveFileNames = getFilenames("*.f2s");

	if (F2Pfilenames.size() == 0u) {
		lineSeparate();
		std::cerr << "No .f2p files found." << std::endl;
	}
	else {
		CreateDirectoryA("Unpacked", NULL);
		for (auto& filename : F2Pfilenames) {
			F2PWrapper f2pWrapper;

			lineSeparate();
			std::cerr << "Begin unpacking " << filename << "..." << std::endl;
			try {
				f2pWrapper.openFile(filename.c_str());
				f2pWrapper.checkHeader();
				f2pWrapper.decryptListing();
				F2PDir root = f2pWrapper.parseF2PListing();
				auto& dataKey = f2pWrapper.isOfficial ?
					keyMap.at(Keys::DATA_OFFICIAL) :
					keyMap.at(Keys::DATA_CUSTOM);

				root.decryptAndWrite(dataKey, std::string("Unpacked\\") +
					filename.substr(0, filename.length() - 4));
			}
			catch (std::exception& e) {
				std::cerr << "Error: " << e.what() << std::endl;
			}
			std::cerr << "Finished unpacking " << filename << std::endl;
		}
	}

	decryptNonF2P(statsFilenames,
		"main stats",
		"Stats",
		Keys::MAIN_STATS,
		MAIN_STATS_HEADER_LEN
		);

	decryptNonF2P(hintBonusStatsFileNames,
		"hint/bonus stats",
		"Stats",
		Keys::SAVES_BONUS_STATS,
		SAVES_BONUS_STATS_HEADER_LEN
		);

	decryptNonF2P(saveFileNames,
		"save",
		"Saves",
		Keys::SAVES_BONUS_STATS,
		SAVES_BONUS_STATS_HEADER_LEN
		);

	return 0;
}
